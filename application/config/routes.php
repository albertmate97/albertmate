<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["restcountries"] = "phpteszt1/getCountries";
$route["arfolyamok"] = "phpteszt1/getArfolyamok";
$route["orszaglista"] = "phpteszt1/getCountries_again";
$route["fetch"] = "phpteszt1/fetch";

