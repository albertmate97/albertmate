<?php

class Phpteszt1 extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model(array("countries","arfolyam","ajaxsearch_model"));
    }

    // 1. Feladat
    public function getCountries(){
        $this->countries->orszagok();
    }

    // 2. Feladat
    public function getArfolyamok(){
        $this->arfolyam->getData();
    }

    // 3. Feladat
    public function getCountries_again(){
        $this->countries->orszagok();
        $this->arfolyam->getData();
        $this->load->view("Countriesview");
        $this->load->view("adat_tabla");
    }

    public function fetch(){
        $output = '';
        $query = '';
        if($this->input->post('query'))
        {
            $query = $this->input->post('query');
        }

        $data = $this->ajaxsearch_model->fetch_data($query);
        $output .= '
                 <thead>
                     <tr>
                       <th>Ország neve</th>
                       <th>Zászlója</th>
                       <th>Fővárosa</th>
                       <th>Népessége</th>
                       <th>Euró átváltás</th>
                     </tr>
                 </thead>
                 <tbody>
        ';
        foreach($data->result() as $row)
        {
            $output .= '
                  <tr>
                   <td>'.$row->name.'</td>
                   <td>'.$row->alpha3Code.'</td>
                   <td>'.$row->capital.'</td>
                   <td>'.$row->population.'</td>
            ';
            if(strcmp($row->currencies_code,"EUR") != 0)
            {
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "phpteszt";


                $conn = new mysqli($servername, $username, $password, $dbname);
                $sql = "SELECT rate FROM arfolyam WHERE currency LIKE '$row->currencies_code' ORDER BY id DESC LIMIT 1";
                $sql2 = mysqli_query($conn, $sql);
                $test = '';
                if(mysqli_num_rows($sql2) > 0) {

                    $asd = mysqli_fetch_array($sql2,MYSQLI_NUM);
                    $test = $asd[0];
                }

                $output .= '
                        <td class="varcode">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Euró átválátás</button>
                            <input type="hidden" value="'.$test.'">
                        </td></tr>
                ';
            }else{
                $output .= '<td></td></tr>';
            }
        }
        $output .= '</tbody>';
        echo $output;
    }
}