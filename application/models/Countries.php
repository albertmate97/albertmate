<?php

class Countries extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function orszagok(){
        $pageRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) &&($_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0' ||  $_SERVER['HTTP_CACHE_CONTROL'] == 'no-cache');
        if($pageRefreshed == 1){
            $this->countries->deleteCountries();
            $json = file_get_contents('https://restcountries.eu/rest/v2/all');
            $obj = json_decode($json);
            for($i = 0; $i < substr_count($json,"{\"name\""); $i++) {
                $json_data = array(
                    "name" => $obj[$i]->name,
                    "alpha2Code" => $obj[$i]->alpha2Code,
                    "alpha3Code" => $obj[$i]->alpha3Code,
                    "capital" => $obj[$i]->capital,
                    "subregion" => $obj[$i]->subregion,
                    "population" => $obj[$i]->population,
                    "currencies_code" => $obj[$i]->currencies[0]->code,
                    "currencies_name" => $obj[$i]->currencies[0]->name,
                    "currencies_symbol" => $obj[$i]->currencies[0]->symbol
                );
                $this->countries->insertCountries($json_data);
            }
        }else{
            $json = file_get_contents('https://restcountries.eu/rest/v2/all');
            $obj = json_decode($json);
            for($i = 0; $i < substr_count($json,"{\"name\""); $i++) {
                $json_data = array(
                    "name" => $obj[$i]->name,
                    "alpha2Code" => $obj[$i]->alpha2Code,
                    "alpha3Code" => $obj[$i]->alpha3Code,
                    "capital" => $obj[$i]->capital,
                    "subregion" => $obj[$i]->subregion,
                    "population" => $obj[$i]->population,
                    "currencies_code" => $obj[$i]->currencies[0]->code,
                    "currencies_name" => $obj[$i]->currencies[0]->name,
                    "currencies_symbol" => $obj[$i]->currencies[0]->symbol
                );
                $this->countries->insertCountries($json_data);
            }
        }
    }

    public function insertCountries($json_data){
        $this->db->insert("countries",$json_data);
    }

    public function deleteCountries(){
        $this->db->query("DELETE FROM countries");
    }
}