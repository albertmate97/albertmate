<?php

class Arfolyam extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function insertXml($xml_data){
        $this->db->insert("arfolyam",$xml_data);
    }

    public function inactivData(){
        $sql = "UPDATE arfolyam SET logikaitorles=CURRENT_TIMESTAMP WHERE logikaitorles IS NULL";

        if ($this->db->query($sql) === TRUE){}
    }

    public function getData(){
        $url = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml?3d9003c7907fa2f15574eb779fd78708";
        $xml = simplexml_load_file($url);

        $count = $xml->Cube->Cube->Cube->count();

        $pageRefreshed = isset($_SERVER['HTTP_CACHE_CONTROL']) &&($_SERVER['HTTP_CACHE_CONTROL'] === 'max-age=0' ||  $_SERVER['HTTP_CACHE_CONTROL'] == 'no-cache');
        if($pageRefreshed == 1) {
            $this->arfolyam->inactivData();
            for ($i = 0; $i < $count; $i++) {
                $xml_data = array(
                    "currency" => $xml->Cube->Cube->Cube[$i]["currency"]->__toString(),
                    "rate" => $xml->Cube->Cube->Cube[$i]["rate"]->__toString()
                );
                $this->arfolyam->insertXml($xml_data);
            }
        }else{
            for ($i = 0; $i < $count; $i++) {
                $xml_data = array(
                    "currency" => $xml->Cube->Cube->Cube[$i]["currency"]->__toString(),
                    "rate" => $xml->Cube->Cube->Cube[$i]["rate"]->__toString()
                );
                $this->arfolyam->insertXml($xml_data);
            }
        }
    }

    public function getAtvaltas($code){
        $data = $code;
        $this->db->select("rate");
        $this->db->from("arfolyam");
        $this->db->where(array(
            "logikaitorles" => NULL,
            "currency" => $data
        ));
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
}