<?php

class Ajaxsearch_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function fetch_data($query){
        $this->db->select("*");
        $this->db->from("countries");
        if($query != '')
        {
            $this->db->like('name', $query);
            $this->db->or_like('alpha3Code', $query);
            $this->db->or_like('capital', $query);
            $this->db->or_like('population', $query);
            $this->db->or_like('currencies_code', $query);
        }

        return $this->db->get();
    }
}